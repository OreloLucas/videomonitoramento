﻿using Microsoft.AspNetCore.Mvc;
using VideoMonitoramento.Application.Interfaces;

namespace VideoMonitoramento.API.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class RecyclerController : ControllerBase
    {
        private readonly IRecyclerService _recyclerService;
        public RecyclerController(IRecyclerService recyclerService)
        {
            _recyclerService = recyclerService;
        }

        [HttpGet, Route ("Process/{days}")]
        public IActionResult Process(short days)
        {
            _recyclerService.DoStart(days);
            return Ok();
        }

        [HttpGet]
        public IActionResult Status()
        {
            var result = _recyclerService.CheckTask();
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }
    }
}
