﻿using Microsoft.AspNetCore.Mvc;
using VideoMonitoramento.Application.Interfaces;
using VideoMonitoramento.Application.Models;

namespace VideoMonitoramento.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ServersController : ControllerBase
    {
        private readonly IServidorService _servidorService;
        private readonly IVideoService _videoService;
        public ServersController(IServidorService servidorService,
            IVideoService videoService)
        {
            _servidorService = servidorService;
            _videoService = videoService;
        }

        [HttpPost]
        public IActionResult NovoServidor(ServidorViewModel servidor)
        {
            var result = _servidorService.Adicionar(servidor);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpDelete]
        public IActionResult RemoveServidor(Guid guid)
        {
            var result = _servidorService.Remover(guid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpPost, Route("{guid}")]
        public IActionResult GetServidor(Guid guid)
        {
            var result = _servidorService.GetServidor(guid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpPost, Route("/available/{guid}")]
        public IActionResult Available(Guid guid)
        {
            var result = _servidorService.Available(guid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpGet]
        public IActionResult ListaServidores()
        {
            var result = _servidorService.List();
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpPost, Route("/{guid}/videos")]
        public IActionResult NovoVideo(Guid guid, VideoViewModel video)
        {
            var result = _videoService.Adicionar(guid, video);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }


        [HttpDelete, Route("/{guid}/videos/{videoGuid}")]
        public IActionResult DeletaVideo(Guid guid, Guid videoGuid)
        {
            var result = _videoService.Remover(guid, videoGuid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpGet, Route("/{guid}/videos/{videoGuid}")]
        public IActionResult GetVideo(Guid guid, Guid videoGuid)
        {
            var result = _videoService.GetVideo(guid, videoGuid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }

        [HttpGet, Route("/{guid}/videos/{videoGuid}/binary")]
        public IActionResult DownloadVideo(Guid guid, Guid videoGuid)
        {
            var result = _videoService.GetVideo(guid, videoGuid);
            return result.Sucesso ? Ok(result.obj.Binary) : BadRequest(result.Mensagem);
        }

        [HttpGet, Route("/{guid}/videos")]
        public IActionResult ListaVideos(Guid guid)
        {
            var result = _videoService.ListaVideos(guid);
            return result.Sucesso ? Ok(result) : BadRequest(result.Mensagem);
        }
    }
}
