using Microsoft.EntityFrameworkCore;
using VideoMonitoramento.Application.Interfaces;
using VideoMonitoramento.Domain.Interfaces;
using VideoMonitoramento.Persistence.Context;
using VideoMonitoramento.Persistence.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IServidorService, ServidorService>();
builder.Services.AddScoped<IVideoService, VideoService>();
builder.Services.AddScoped<IServidorRepository, ServidorRepository>();
builder.Services.AddScoped<IVideoRepository, VideoRepository>();

builder.Services.AddSingleton<IRecyclerService, RecyclerService>();
builder.Services.AddSingleton<IRecyclerRepository, RecyclerRepository>();


IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();



builder.Services.AddDbContext<GeralContext>(
    o =>
    {
        o.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
    });
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
