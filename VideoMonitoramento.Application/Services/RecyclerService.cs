﻿using ideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Interfaces;

namespace VideoMonitoramento.Application.Interfaces
{
    public class RecyclerService : IRecyclerService
    {
        private Thread t;
        private short _days;
        private readonly IRecyclerRepository _recyclerRepository;

        public RecyclerService(IRecyclerRepository recyclerRepository)
        {
            _recyclerRepository = recyclerRepository;
            t = new Thread(new ThreadStart(Exec));
        }

        public ReturnType<bool> CheckTask()
        {
            try
            {
                return t.IsAlive ?
               new ReturnType<bool>() { Mensagem = "executando", Sucesso = true, obj = true } :
               new ReturnType<bool>() { Mensagem = "parado", Sucesso = true, obj = false };
            }
            catch (Exception)
            {
                return new ReturnType<bool>() { Mensagem = "erro ao recuperar status", Sucesso = false, obj = false };
            }
        }

        public void DoStart(short days)
        {
            _days = days;
            if (!t.IsAlive)
                t.Start();
        }

        private void Exec()
        {
            Task.Delay(1000).Wait();
            _recyclerRepository.Recycle(_days);
            Task.Delay(1000).Wait();
            t = new Thread(new ThreadStart(Exec));
        }
    }
}
