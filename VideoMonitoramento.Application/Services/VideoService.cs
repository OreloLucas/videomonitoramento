﻿using ideoMonitoramento.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Domain.Interfaces;

namespace VideoMonitoramento.Application.Interfaces
{
    public class VideoService : IVideoService
    {
        private readonly IVideoRepository _videoRepository;

        public VideoService(IVideoRepository videoService)
        {
            _videoRepository = videoService;
        }

        public ReturnType<Video> Adicionar(Guid guid, VideoViewModel video)
        {
            var v = _videoRepository.Adicionar(new Video()
            {
                IdServidor = guid,
                Descricao = video.Descricao,
                Binary = video.Binary,
            });

            return v is null ?
                new ReturnType<Video>() { Sucesso = false, Mensagem = "erro incluindo video" } :
                new ReturnType<Video>() { Sucesso = true, Mensagem = "ok", obj = v };
        }

        public ReturnType<Video> GetVideo(Guid guidServidor, Guid videoGuid)
        {
            var v = _videoRepository.GetVideo(guidServidor, videoGuid);

            return v is null ?
                new ReturnType<Video>() { Sucesso = false, Mensagem = "erro recupernado video" } :
                 new ReturnType<Video>() { Sucesso = true, Mensagem = "ok", obj = v };
        }

        public ReturnType<List<Video>> ListaVideos(Guid guidServidor)
        {
            var v = _videoRepository.ListaVideos(guidServidor);
            return v is null ?
                new ReturnType<List<Video>>() { Sucesso = false, Mensagem = "erro ao listar servidores" } :
                new ReturnType<List<Video>>()
                {
                    Sucesso = true,
                    Mensagem = "ok",
                    obj = v
                };
        }


        public ReturnType<Video> Remover(Guid guidServidor, Guid videoGuid)
        {
            var v = _videoRepository.RemoveVideo(guidServidor, videoGuid);

            return v ?
                new ReturnType<Video>() { Sucesso = true, Mensagem = "Video removido!" } :
                 new ReturnType<Video>() { Sucesso = false, Mensagem = "erro removendo video" };
        }
    }
}
