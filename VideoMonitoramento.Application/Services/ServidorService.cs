﻿using ideoMonitoramento.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Domain.Interfaces;

namespace VideoMonitoramento.Application.Interfaces
{
    public class ServidorService : IServidorService
    {
        private readonly IServidorRepository _servidorRepository;
        public ServidorService(IServidorRepository servidorRepository)
        {
            _servidorRepository = servidorRepository;
        }

        public ReturnType<Servidor> Adicionar(ServidorViewModel servidor)
        {
            var s = _servidorRepository.Adicionar(new Servidor()
            {
                Nome = servidor.Nome,
                IP = servidor.IP,
                Porta = servidor.Porta,
            });

            return s is null ?
                new ReturnType<Servidor>() { Sucesso = false, Mensagem = "erro ao adiconar servidor" } :
                new ReturnType<Servidor>() { Sucesso = true, Mensagem = "ok", obj = s };

        }

        public ReturnType<Servidor> Available(Guid guid)
        {
            try
            {
                var client = new TcpClient("google.com", 80);
                return new ReturnType<Servidor>() { Mensagem="ok", Sucesso = true};
            }
            catch (Exception ex)
            {
                return new ReturnType<Servidor>() { Mensagem = ex.Message, Sucesso = false };
            }
        }

        public ReturnType<Servidor> GetServidor(Guid guid)
        {
            var s = _servidorRepository.GetServidor(guid);
            return s is null ?
                new ReturnType<Servidor>() { Sucesso = false, Mensagem = "erro ao recuperar servidor" } :
                new ReturnType<Servidor>() { Sucesso = true, Mensagem = "ok", obj = s };
        }

        public ReturnType<List<Servidor>> List()
        {
            var s = _servidorRepository.ListaServidor();
            return s is null ?
                new ReturnType<List<Servidor>>() { Sucesso = false, Mensagem = "erro ao listar servidores" } :
                new ReturnType<List<Servidor>>() { Sucesso = true, Mensagem = "ok", obj = s };
        }

        public ReturnType<Servidor> Remover(Guid guid)
        {
            return _servidorRepository.RemoveServidor(guid) ?
                new ReturnType<Servidor>() { Sucesso = true, Mensagem = "ok" } :
                new ReturnType<Servidor>() { Sucesso = false, Mensagem = "erro ao remover servidor" };

        }
    }
}
