﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoMonitoramento.Application.Models
{
    public class ServidorViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string IP { get; set; }
        public int Porta { get; set; }
    }
}
