﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoMonitoramento.Application.Models
{
    public class VideoViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Byte[] Binary { get; set; }
    }
}
