﻿using ideoMonitoramento.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Application.Interfaces
{
    public interface IRecyclerService
    {

        void DoStart(short days);
        ReturnType<bool> CheckTask();
    }
}
