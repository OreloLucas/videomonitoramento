﻿using ideoMonitoramento.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Application.Interfaces
{
    public interface IVideoService
    {
        ReturnType<Video> Adicionar(Guid guid, VideoViewModel video);
        ReturnType<Video> Remover(Guid guidServidor, Guid videoGuid);
        ReturnType<Video> GetVideo(Guid guidServidor, Guid videoGuid);
        ReturnType<List<Video>> ListaVideos(Guid guidServidor);
    }
}
