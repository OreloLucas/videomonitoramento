﻿using ideoMonitoramento.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Application.Models;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Application.Interfaces
{
    public interface IServidorService
    {
        ReturnType<Servidor> Adicionar(ServidorViewModel servidor);
        ReturnType<Servidor> Remover(Guid guid);
        ReturnType<Servidor> GetServidor(Guid guid);
        ReturnType<Servidor> Available(Guid guid);
        ReturnType<List<Servidor>> List();
    }
}
