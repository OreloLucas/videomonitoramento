CREATE TABLE public.video (
	"Id" uuid NOT NULL DEFAULT gen_random_uuid(),
	descricao varchar NULL,
	"binary" bytea NULL,
	idservidor uuid NOT NULL,
	"data" date NOT NULL DEFAULT CURRENT_DATE
);
ALTER TABLE public.video ADD CONSTRAINT video_fk FOREIGN KEY (idservidor) REFERENCES public.servidor("Id");


CREATE TABLE public.servidor (
	"Id" uuid NOT NULL DEFAULT gen_random_uuid(),
	nome varchar NULL,
	ip varchar NULL,
	porta int4 NULL,
	CONSTRAINT servidor_pkey PRIMARY KEY ("Id")
);