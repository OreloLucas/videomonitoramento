﻿using Microsoft.EntityFrameworkCore;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Persistence.Mapping;

namespace VideoMonitoramento.Persistence.Context
{
    public class GeralContext : DbContext
    {
        public bool lazyload = false;

        public GeralContext(DbContextOptions<GeralContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ServidorMapping());
            builder.ApplyConfiguration(new VideoMapping());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(lazyload);

        }


       public DbSet<Servidor> Servidor { get; set; }
       public DbSet<Video> Video { get; set; }
    }
}
