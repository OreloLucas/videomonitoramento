﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Domain.Interfaces;
using VideoMonitoramento.Persistence.Context;

namespace VideoMonitoramento.Persistence.Repositories
{
    public class ServidorRepository : IServidorRepository
    {
        private readonly GeralContext _context;

        public ServidorRepository(GeralContext context)
        {
            _context = context;
        }

        public Servidor? Adicionar(Servidor servidor)
        {
            try
            {
                _context.Servidor.Add(servidor);
                _context.SaveChanges();
                return servidor;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Servidor GetServidor(Guid id)
        {
            _context.lazyload = true;
            return _context.Servidor.Where(x => x.Id.Equals(id)).FirstOrDefault();
        }

        public List<Servidor> ListaServidor()
        {
            _context.lazyload = true;
            return _context.Servidor.ToList();
        }

        public bool RemoveServidor(Guid id)
        {
            try
            {
                var servidor = GetServidor(id);
                _context.Remove(servidor);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
