﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Domain.Interfaces;
using VideoMonitoramento.Persistence.Context;

namespace VideoMonitoramento.Persistence.Repositories
{
    public class RecyclerRepository : IRecyclerRepository
    {
        private readonly GeralContext _context;

        public RecyclerRepository(IServiceScopeFactory services)
        {
            _context = services.CreateScope().ServiceProvider.GetService<GeralContext>();           
        }

        public void Recycle(short days)
        {
            var videos = _context.Video.Where(x => x.data <= DateTime.Now.AddDays(-days).Date).ToList();
            foreach (var video in videos)
            {
                _context.Remove(video);
                _context.SaveChanges();
            }
        }

    }
}
