﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;
using VideoMonitoramento.Domain.Interfaces;
using VideoMonitoramento.Persistence.Context;

namespace VideoMonitoramento.Persistence.Repositories
{
    public class VideoRepository : IVideoRepository
    {
        private readonly GeralContext _context;

        public VideoRepository(GeralContext context)
        {
            _context = context;
        }

        public Video? Adicionar(Video video)
        {
            try
            {
                video.data = DateTime.Now;
                _context.Video.Add(video);
                _context.SaveChanges();
                return video;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Video GetVideo(Guid id)
        {
            throw new NotImplementedException();
        }

        public Video GetVideo(Guid idServidor, Guid idVideo) => _context.Video.Where(x => x.Id.Equals(idVideo) && x.IdServidor.Equals(idServidor)).FirstOrDefault();

        public List<Video> ListaVideo()
        {
            throw new NotImplementedException();
        }

        public List<Video> ListaVideos(Guid idServidor)
        => _context.Video.Where(x => x.IdServidor.Equals(idServidor)).ToList();

        public void Recycle(short days)
        {
            var videos = _context.Video.Where(x => x.data <= DateTime.Now.AddDays(-days).Date).ToList();
            foreach (var video in videos)
            {
                _context.Remove(video);
                _context.SaveChanges();
            }
        }

        public bool RemoveVideo(Guid idServidor, Guid idVideo)
        {
            try
            {
                var video = _context.Video.Where(x => x.Id.Equals(idVideo) && x.IdServidor.Equals(idServidor)).FirstOrDefault();
                if (video is null)
                    return false;

                _context.Remove(video);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
