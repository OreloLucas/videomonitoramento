﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Persistence.Mapping
{
    public class VideoMapping : IEntityTypeConfiguration<Video>
    {
        public void Configure(EntityTypeBuilder<Video> builder)
        {
            builder.ToTable("video");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Descricao).HasColumnName("descricao").HasColumnType("varchar");
            builder.Property(x => x.IdServidor).HasColumnName("idservidor").HasColumnType("uuid");
            builder.Property(x => x.Binary).HasColumnName("binary").HasColumnType("bytea");
            builder.Property(x => x.data).HasColumnName("data").HasColumnType("date");
            
        }
    }
}
