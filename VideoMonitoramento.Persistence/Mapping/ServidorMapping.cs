﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Persistence.Mapping
{
    public class ServidorMapping : IEntityTypeConfiguration<Servidor>
    {
        public void Configure(EntityTypeBuilder<Servidor> builder)
        {
            builder.ToTable("servidor");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Nome).HasColumnName("nome").HasColumnType("varchar");
            builder.Property(x => x.IP).HasColumnName("ip").HasColumnType("varchar");
            builder.Property(x => x.Porta).HasColumnName("porta").HasColumnType("int");

            builder.HasMany(x => x.Videos).WithOne("Servidor").HasForeignKey(x => x.IdServidor);
        }
    }
}
