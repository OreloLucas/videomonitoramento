﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Domain.Interfaces
{
    public interface IServidorRepository
    {

        Servidor? Adicionar(Servidor servidor);
        bool RemoveServidor(Guid id);
        Servidor GetServidor(Guid id);
        List<Servidor> ListaServidor();
    }
}
