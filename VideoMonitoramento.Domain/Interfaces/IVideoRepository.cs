﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Domain.Interfaces
{
    public interface IVideoRepository
    {

        Video? Adicionar(Video video);
        bool RemoveVideo(Guid idServidor, Guid idVideo);
        Video GetVideo(Guid idServidor, Guid idVideo);
        List<Video> ListaVideos(Guid idServidor);
    }
}
