﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoMonitoramento.Domain.Entities;

namespace VideoMonitoramento.Domain.Interfaces
{
    public interface IRecyclerRepository
    {
        void Recycle(short days);
    }
}
