﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoMonitoramento.Domain.Entities
{
    public class Servidor
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int Porta { get; set; }
        public string IP { get; set; }

        public virtual List<Video> Videos { get; set; } = new List<Video>();
    }
}
