﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoMonitoramento.Domain.Entities
{
    public class Video
    {
        public Guid Id { get; set; }
        public Guid IdServidor { get; set; }
        public string Descricao { get; set; }
        public byte[] Binary { get; set; }
        public DateTime data { get; set; }
        protected virtual Servidor Servidor { get; set; }
    }
}
